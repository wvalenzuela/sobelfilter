Sample: SobelFilter
Minimum spec: SM 2.0

This sample implements the Sobel edge detection filter for 8-bit monochrome images.

Key concepts:
Graphics Interop
Image Processing

#----------------------------------

This is a copy of the SobelFilter example of CUDA library. I created the cmake implementation. It was tested on XCODE 7.0 and CMake 3.4.

Warning, to compile it on Xcode you have to select the project SobelFilter to run it, but you have to add the environment variable in the Edit Scheme section first: DYLD_LIBRARY_PATH /Developer/NVIDIA/CUDA-7.5/samples/common